import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-admin',
  templateUrl: './page-admin.component.html',
  styleUrls: ['./page-admin.component.css']
})
export class PageAdminComponent implements OnInit {
  findusers : boolean=false;
  deleteuser : boolean=false;
  createarticle : boolean=false;
  updatearticle: boolean=false;
  delarticles : boolean=false;
admin:{nom:string, prenom:string}

  constructor() { }

  ngOnInit(): void {
   this.admin= JSON.parse(sessionStorage.getItem("admin"));
  }

  OnModif1(){
    this.findusers = true;
    this.deleteuser = false;
    this.createarticle = false;
    this.updatearticle = false;
    this.delarticles = false;
  }
  OnModif2(){


    this.findusers = false;
    this.deleteuser = true;
    this.createarticle = false;
    this.updatearticle = false;
    this.delarticles = false;
  }
  OnModif3(){
    this.createarticle = true;
    this.findusers = false;
    this.deleteuser = false;

    this.updatearticle = false;
    this.delarticles = false;
  }
  OnModif4(){
    this.updatearticle = true;
    this.findusers = false;
    this.deleteuser = false;
    this.createarticle = false;

    this.delarticles = false;
  }
  OnModif5(){
    this.delarticles = true
    this.findusers = false;
    this.deleteuser = false;
    this.createarticle = false;
    this.updatearticle = false;

  }
}
